import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './root/components/page-not-found.component';


const routes: Routes = [
  {
    path:'',
    children: [
      {path: '', redirectTo: '/my-form', pathMatch: 'full'},
      {path: '**', component: PageNotFoundComponent},
      {
        path: 'my-form',
        loadChildren: 'app/my-form/my-form.module#MyFormModule',
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

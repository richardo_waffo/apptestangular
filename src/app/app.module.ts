import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router'

import 'hammerjs';

import {MatInputModule, MatButtonModule} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from '../app/root/components/app.component';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import { PageNotFoundComponent } from './root/components/page-not-found.component';
import { environment } from '../environments/environment';
import {MyFormComponent} from './my-form/components/my-form.component';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    MyFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

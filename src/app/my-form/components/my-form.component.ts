import { Component, OnInit } from '@angular/core';

import {Address} from '../../address';

@Component({
  selector: 'my-form',
  templateUrl: '../html/my-form.component.html',
  styleUrls: ['../styles/my-form.component.css']
})
export class MyFormComponent implements OnInit {

    address = new Address();

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
      alert("Thank for submitting! Data: " + JSON.stringify(this.address));
  }

}

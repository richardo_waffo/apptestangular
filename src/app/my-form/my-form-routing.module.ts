import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {MyFormComponent} from './components/my-form.component';

const routes: Routes = [
  {
    path: '', component: MyFormComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyFormRoutingModule { }

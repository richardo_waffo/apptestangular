import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpModule, JsonpModule} from '@angular/http'
import { FormsModule } from '@angular/forms'
import {MatInputModule, MatButtonModule, } from '@angular/material'

import { MyFormRoutingModule } from './my-form-routing.module';

import {MyFormComponent} from './components/my-form.component';

@NgModule({
  imports: [
    CommonModule,
    MyFormRoutingModule,
    HttpModule,
    JsonpModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
  ],
  declarations: [MyFormComponent]
})
export class MyFormModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: '../html/app.component.html',
  styleUrls: ['../styles/app.component.scss']
})
export class AppComponent implements OnInit {
   constructor(){}

   ngOnInit() {

   }
   title = 'E-Commerce';
  
}
